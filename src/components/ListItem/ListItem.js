import React from 'react';

import style from './ListItem.module.css';

const listItem = props => {
    const Item = 
    <div 
        className={style.ListItem}
        onClick={props.clicked}
    >
        <div className={style.Avatar}>
            <img src={props.avatar} alt="avatar" />
        </div>
        <div className={style.FullName}>{props.firstName} {props.lastName}</div>
        {props.checked ? <input type="checkbox" checked/> : <input type="checkbox"/> }
    </div>;

    return Item;
}

export default listItem;