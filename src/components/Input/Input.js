import React from 'react';

import style from './Input.module.css';

const input = props => {
    let inputElement = <input className={style.Input}
    {...props.inputConfig}
    value={props.value}
    onChange={props.changed} />;

    return (
        <div>
            <div className={style.Label}>Contact</div>
            {inputElement}
        </div>
    );
}

export default input;