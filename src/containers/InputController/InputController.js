import React, { useState } from 'react';

import Input from '../../components/Input/Input';
import List from '../List/List';

const InputController = props => {
    const [inputValue, setInputValue] = useState('');
    const inputConfig = {type: 'text', placeholder: 'Search'};

    const inputChangedHandler = (event) => {
        setInputValue(event.target.value);
    }

    return(
        <React.Fragment>
            <Input 
                inputConfig={inputConfig} 
                inputValue={inputValue}
                changed={(event) => inputChangedHandler(event)}
            />
            <List inputValue={inputValue} />
        </React.Fragment>
    );
}

export default InputController;