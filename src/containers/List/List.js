import React, { useState, useEffect } from 'react';

import axios from 'axios';

import ListItem from '../../components/ListItem/ListItem';
import Spinner from '../../components/Spinner/Spinner';

const List = props => {
    const [list, setList] = useState([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        axios.get('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json')
        .then(res => {
            const prepareList = new Promise((resolve, rject) => {
                let sortedList = res.data.sort((a, b) =>{
                    if(a.last_name < b.last_name) { return -1; }
                    if(a.last_name > b.last_name) { return 1;}
                    return 0;
                });
                console.log(sortedList);
                resolve(sortedList);
            });

            prepareList
            .then(result => {
                const listWithChecked = result.map(obj =>({...obj, checked: false}));
                return listWithChecked;
            })
            .then(result => {
                setList(result);
                setLoading(false);
            });
        })
        .catch(err => {
            setLoading(false);
            setError(true);
        });
    }, []);

    const onClickedHandler = (listItemIdentifier) => {
        const showCheckedInConsole = new Promise((resolve, reject)=>{
            if(list[listItemIdentifier].checked){
                const updatedItem = {...list[listItemIdentifier], checked: false};
                const updatedList = {
                    ...list,
                    [listItemIdentifier]: updatedItem
                }
                resolve(updatedList);
            }
            else {
                const updatedItem = {...list[listItemIdentifier], checked: true};
                const updatedList = {
                    ...list,
                    [listItemIdentifier]: updatedItem
                }
                resolve(updatedList);
            }
        })

        showCheckedInConsole
        .then(res =>{
            setList(res);
            return res;
        })
        .then(res => {
            for(let item in res){
                if(res[item].checked){
                    console.log(res[item].id);
                }              
            }
        });
    };
    let displayList = <Spinner />;
    if(!loading && !error){
        displayList = Object.keys(list).map(listItem => {
            if(!props.inputValue){
                return(
                    <ListItem 
                        key={listItem}
                        avatar={list[listItem].avatar}
                        firstName={list[listItem].first_name}
                        lastName={list[listItem].last_name}
                        clicked={() => onClickedHandler(listItem)}
                        checked={list[listItem].checked}
                    />
                );
            }
            else if(props.inputValue && ((props.inputValue === list[listItem].first_name) || (props.inputValue === list[listItem].last_name))){
                return(
                    <ListItem 
                        key={listItem}
                        avatar={list[listItem].avatar}
                        firstName={list[listItem].first_name}
                        lastName={list[listItem].last_name}
                        clicked={() => onClickedHandler(listItem)}
                        checked={list[listItem].checked}
                    />
                );
            }
            else return null;
        });
    }
    else if(!loading && error){
        displayList = <p>Something went wrong! Try again later</p>
    }
    

    return(
        <div>
            {displayList}
        </div>
    );
}

export default List;