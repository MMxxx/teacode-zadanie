import React from 'react';
import './App.css';

import InputController from './containers/InputController/InputController';

const App = props => {
  return (
    <div className="App">
      <InputController />
    </div>
  );
}

export default App;
